use palette::Hsv;
use rand::Rng;
use rayon::prelude::*;
use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

struct Pixel {
    pix: Hsv,
    lit: bool,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(&config, vec!["colors", "step", "threshold"]);

    let mut rng = rand::rng();
    let mut pixels: Vec<Pixel> =
        initializer::luma(config.lights.count(), 0.0)
        .as_slice()
        .par_iter()
        .map(|x| Pixel { pix: *x, lit: false, wax: false })
        .collect();

    let dmx = output::e131::Dmx::new();
    
    // setup structures for selecting random color values
    // sort them so that low < high, or else rng.random_range gets grumpy
    let mut sort_h = [
        params.colors[0].hue.into_positive_degrees(),
        params.colors[1].hue.into_positive_degrees()
    ];
    let _ = &sort_h.sort_by(|a, b| a.partial_cmp(b).unwrap());
    if sort_h[0] == sort_h[1] {
        sort_h[0] -= 0.001;
    }
    //println!("h: {:?}", sort_h);
    let mut sort_s = [
        params.colors[0].saturation,
        params.colors[1].saturation
    ];
    let _ = &sort_s.sort_by(|a, b| a.partial_cmp(b).unwrap());
    if sort_s[0] == sort_s[1] {
        sort_s[0] -= 0.001;
    }
    //println!("s: {:?}", sort_s);
    let mut sort_v = [
        params.colors[0].value,
        params.colors[1].value
    ];
    let _ = &sort_v.sort_by(|a, b| a.partial_cmp(b).unwrap());
    if sort_v[0] == sort_v[1] {
        sort_v[0] -= 0.001;
    }
    //println!("v: {:?}", sort_v);
    
    loop {
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);
        for pixel in pixels.as_mut_slice() {
            // already lit?  ramp up or down.
            if pixel.lit {
                if pixel.wax {
                    pixel.pix.value += rng.random_range(0.0 .. params.step);
                    if pixel.pix.value >= config.output.max_intensity {
                        pixel.wax = false;
                    }
                }
                else {
                    pixel.pix.value -= rng.random_range(0.0 .. params.step);
                    if pixel.pix.value <= 0.0 {
                        pixel.pix.value = 0.0;
                        pixel.lit = false;
                        pixel.wax = false;
                    }
                }
            }            
            // random chance to light it
            else {
                // random level to start
                if  rng.random::<f32>() < params.threshold {
                    pixel.lit = true;
                    pixel.wax = true;
                    // pick H, S, V in the range specified by supplied colors
                    let h = rng.random_range(sort_h[0] .. sort_h[1]);
                    let s = rng.random_range(sort_s[0] .. sort_s[1]);
                    let v = rng.random_range(sort_v[0] .. sort_v[1]);
                    //println!("h {}  s {}  v {}", h, s, v);
                    pixel.pix = Hsv::new(h, s, v);
                }
            }
        }
        
        sleep(params.sleep);
    }
}
