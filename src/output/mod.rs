pub mod console {
    use rayon::prelude::*;
    use ansi_term::{ANSIString, ANSIStrings, Style};
    use ansi_term::Colour::{RGB, White};
    use palette::{FromColor, Hsv, Srgb};

    use crate::config;
    
    pub fn output( pixels: &[Hsv], config: &config::Config ) {
        if !config.output.console {
            return;
        }
        let strings = pixels.par_iter()
            .map(|pixel| {
                let srgb = Srgb::from_color(*pixel);
                let rgb = RGB(
                    (srgb.red * 255.0) as u8,
                    (srgb.green * 255.0) as u8,
                    (srgb.blue * 255.0) as u8
                );
                Style::new().on(rgb).fg(White).paint(" ")
            })
            .collect::<Vec<ANSIString<'static>>>();
        print!("{}\r", ANSIStrings(&strings));
    }
}

pub mod e131 {
    use rayon::prelude::*;
    use palette::{FromColor, Hsv, Srgb};
    use sacn::DmxSource;
    
    use crate::config;

    pub struct Dmx {
        source: DmxSource
    }
    impl Dmx {
        pub fn new() -> Dmx {
            Dmx { source: DmxSource::new("Controller").unwrap() }
        }
    }
    impl Drop for Dmx {
        fn drop(&mut self) {
            let _res = self.source.terminate_stream(1);
        }
    }

    // gamma correction map
    // https://learn.adafruit.com/led-tricks-gamma-correction/the-quick-fix
    static MAP: &[u8; 256] = &[
          0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
          0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
          1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
          2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
          5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
         10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
         17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
         25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
         37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
         51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
         69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
         90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
        115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
        144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
        177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
        215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255
    ];

    pub fn gamma_corrected(hsv: Hsv) -> Srgb {
        let srgb = Srgb::from_color(hsv);
        let gamma = Srgb::new(
            MAP[(srgb.red * 255.0) as usize],
            MAP[(srgb.green * 255.0) as usize],
            MAP[(srgb.blue * 255.0) as usize]
        );
        gamma.into_format()
    }

    pub fn output( dmx: &Dmx, pixels: &[Hsv], config: &config::Config ) {
        if !config.output.e131 {
            return;
        }
        let universes = build_universes(pixels, config);
        for (universe, lights) in universes.iter().enumerate() {
            let _res = dmx.source.send((universe + 1) as u16, lights);
        }
    }

    fn build_universes(pixels: &[Hsv], config: &config::Config) -> Vec<Vec<u8>> {
        let mut out: Vec<u8> = pixels.par_iter()
            .flat_map(|p| {
                let srgb: Srgb<u8> = gamma_corrected(*p).into_format();
                [srgb.red, srgb.green, srgb.blue]
            })
            .collect::<Vec<u8>>();
        let mut universes = Vec::new();
        while out.len() > config.output.universe_size {
            let u = out.split_off(config.output.universe_size);
            universes.push(out);
            out = u;
        }
        universes.push(out);
        universes
    }

    #[test]
    fn test_build_universes() {
        // dummy up a config struct to have just the 3 pixels we are testing
        let config = config::Config {
            lights: config::Lights {
                zones: vec![
                    config::Zone {
                        sections: vec![
                            config::Section {
                                label: 0,
                                head: 0,
                                pixels: 3,
                                tail: 0,
                            },
                        ],
                    },
                ],
            },
            output: config::Output {
                console: false,
                e131: false,
                universe_size: 510,
                max_intensity: 1.0,
            }
        };
        let pixels = vec![
            Hsv::from_components((0.0, 1.0, 1.0)),    // red
            Hsv::from_components((120.0, 1.0, 1.0)),  // green
            Hsv::from_components((240.0, 1.0, 1.0)),  // blue
        ];
        let universes = build_universes(pixels.as_slice(), &config);
        // expecting vec of u8 with simple red/blue/green
        // nested in a vec of universes
        assert_eq!(universes, vec![vec![255, 0, 0, 0, 255, 0, 0, 0, 255]]);
    }
}
