use std::thread::sleep;
use std::time::Instant;

use palette::Hsv;
use rand::prelude::*;
use rayon::prelude::*;

mod config;
mod initializer;
mod output;
mod params;

#[derive(Debug)]
struct Pixel {
    pix: Hsv,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(&config, vec!["colors"]);
    // take redundant value from end of vec
    let mut colors = params.colors.clone();
    colors.pop();

    let min_val = 0.05;
    let max_val = 1.0;
    let mut rng = rand::rng();
    let mut pixels = (0 .. config.lights.count())
        .map(|_i| {
            let mut color = *colors.choose(&mut rng).unwrap();
            color.value = rng.random_range(min_val .. max_val);
            Pixel {
                pix: color,
                wax: rand::random(),
            }
        })
        .collect::<Vec<Pixel>>();
             
    let dmx = output::e131::Dmx::new();
    let step = 0.005;
    loop {
        let now = Instant::now();
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);
        for pixel in pixels.as_mut_slice() {
            if pixel.wax {
                pixel.pix.value += step;
                if pixel.pix.value >= max_val {
                    pixel.wax = false;
                }
            }
            else {
                pixel.pix.value -= step;
                if pixel.pix.value <= min_val {
                    pixel.wax = true;
                }
            }
        }
        
        sleep(params.sleep - now.elapsed());
    }
}
