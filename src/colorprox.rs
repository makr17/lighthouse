use std::f32::consts::PI;

use palette::{Hsv, RgbHue};
use rand::Rng;
use rayon::prelude::*;
use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

struct Pixel {
    pix: Hsv,
    lit: bool,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(
        &config,
        vec!["radius", "step", "threshold"]
    );
    let mut pixels: Vec<Pixel> =
        initializer::luma(config.lights.count(), 0.0)
        .as_slice()
        .par_iter()
        .map(|x| Pixel { pix: *x, lit: false, wax: false })
        .collect();

    let mut rng = rand::rng();
    let dmx = output::e131::Dmx::new();
    loop {
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);

        for i in 0..pixels.len() {
            let mut back = i as i64 - params.radius as i64;
            if back < 0 {
                back = 0;
            }
            let mut forward = i + params.radius;
            if forward >= pixels.len() {
                forward = pixels.len() - 1;
            }
            let modifier: u32 = pixels[(back as usize)..(forward)]
                .par_iter()
                .map(|x| if x.lit { 1 } else { 0 })
                .sum();
            let chance = params.threshold * (modifier + 1) as f32;
            //println!("{} : {} : {} => {}", back, i, forward, chance);
            // already lit?  ramp up or down.
            if pixels[i].lit {
                if pixels[i].wax {
                    pixels[i].pix.value += rng.random_range(0.0 .. params.step);
                    if pixels[i].pix.value >= config.output.max_intensity {
                        pixels[i].wax = false;
                    }
                }
                else {
                    pixels[i].pix.value -= rng.random_range(0.0 .. params.step);
                    if pixels[i].pix.value <= 0.0 {
                        pixels[i].pix.value = 0.0;
                        pixels[i].lit = false;
                        pixels[i].wax = false;
                    }
                }
            }            
            // random chance to light it
            else {
                // random level to start
                if  rng.random::<f32>() < chance {
                    // average neighboring pixels (within the radius)                    
                    let thispix = average_pixels(&pixels[(back as usize)..(forward)]);
                    if thispix.pix.saturation > 0.0 {
                        pixels[i] = thispix;
                    }
                    // fall back to random hue and saturation if no lit neighbors
                    else {
                        pixels[i].pix.hue = RgbHue::from_degrees(rng.random_range(-180.0 .. 180.0));
                        pixels[i].pix.saturation = rng.random_range(0.7 .. 1.0);
                    }
                    // set random intensity within the step range
                    pixels[i].pix.value = rng.random_range(0.0 .. params.step);
                    // light the pixel
                    pixels[i].lit = true;
                    // and mark is as waxing (going up)
                    pixels[i].wax = true;
                }
            }
        }        

        sleep(params.sleep);
    }
}

fn average_pixels( p: &[Pixel] ) -> Pixel {
    let mut x: f32 = 0.0;
    let mut y: f32 = 0.0;
    let mut s: f32 = 0.0;
    let mut v: f32 = 0.0;
    let mut count: f32 = 0.0;
    for pix in p {
        // skip dark pixels
        if !pix.lit {
            continue;
        };
        // average x and y for the hue angle
        x += (pix.pix.hue.into_degrees() / 180.0 * PI).cos();
        y += (pix.pix.hue.into_degrees() / 180.0 * PI).sin();
        // take max saturation
        if pix.pix.saturation > s {
            s = pix.pix.saturation;
        }
        // and max value
        if pix.pix.value > v {
            v = pix.pix.value;
        }
        count += 1.0;
    }
    let avg_hue = (y/count).atan2(x/count) * 180.0 / PI;
    let avg = Hsv::from_components((avg_hue, s, v));
    Pixel { pix: avg, lit: false, wax: false }
}
