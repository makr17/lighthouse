use palette::{Hsv, RgbHue};
use rand::Rng;
use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

#[derive(Debug)]
struct Particle {
    position: f32,
    mass:     f32,
    velocity: f32,
    hue:      RgbHue,
}

fn main () {
    let config = config::new();
    let params = params::new(
        &config,
        vec![]
    );
    let mut pixels: Vec<Hsv> = initializer::luma(config.lights.count(), 0.0);

    let pcount: usize = 10;
    let starting_momentum = 1.0;
    let mut particles: Vec<Particle> = vec![];
    let mut rng = rand::rng();
    for _i in  0..pcount {
        let position: f32 = rng.random_range(0.0 .. pixels.len() as f32);
        let mass: f32 = rng.random_range(0.25 .. 0.95);
        let velocity = starting_momentum/mass;
        particles.push(
            Particle {
                position,
                mass,
                velocity,
                hue: RgbHue::from_degrees(rng.random_range(-180.0 .. 180.0)),
            }
        );
    }
    particles.sort_by(|a, b| a.position.partial_cmp(&b.position).unwrap());
    
    let dmx = output::e131::Dmx::new();
    loop {
        output::console::output(&pixels, &config);
        output::e131::output(&dmx, &pixels, &config);
        particles.sort_by(|a, b| a.position.partial_cmp(&b.position).unwrap());
        for p in &particles {
            let mut pos = p.position;
            pixels[pos as usize].value = 0.0;
            pos += p.velocity;
            pixels[pos as usize].hue = p.hue;
            pixels[pos as usize].value = p.mass;
        }
        sleep(params.sleep);
    }
}
