use enterpolation::{linear::Linear, Curve};
use palette::{FromColor, GammaLuma, Hsv};

#[allow(dead_code)]
pub fn gradient (count: usize, colors: Vec<Hsv>) -> Vec<Hsv> {
    let hues = colors.iter()
        .map(|x| x.hue.into_positive_degrees())
        .collect::<Vec<f32>>();
    // take mean saturation and value from provided colors
    let saturation = colors.iter()
        .map(|x| x.saturation)
        .sum::<f32>() / colors.len() as f32;
    let value = colors.iter()
        .map(|x| x.value)
        .sum::<f32>() / colors.len() as f32;
    // build a linear gradient for each pair of colors in the input
    // TODO: integer division error/drift on the number of resulting pixels
    let piece_count = count / (hues.len() - 1);
    let mut pixels: Vec<Hsv> = vec![];
    for idx in 1..hues.len() {
        let from = hues[idx-1];
        let to = hues[idx];
        let gradient = Linear::builder()
            .elements([from, to])
            .equidistant::<f32>()
            .normalized()
            .build()
            .unwrap();
        let mut piece: Vec<Hsv> = gradient.take(piece_count + 1)
            .map(|x| Hsv::new(x, saturation, value))
            .collect::<Vec<Hsv>>();
        // don't duplicate the start/end colors of adjacent pieces
        piece.pop();
        pixels.append(&mut piece);
    }

    pixels
}

#[allow(dead_code)]
pub fn luma (count: usize, level: f32) -> Vec<Hsv> {
    let mut pixels = vec![];
    for _i in 0..count {
        pixels.push(
            Hsv::from_color(GammaLuma::new(level))
        );
    }
    pixels
}    
