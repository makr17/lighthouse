use std::thread::sleep;

use palette::RgbHue;
use rand::Rng;

mod config;
mod initializer;
mod output;
mod params;

#[derive(Clone, Debug)]
struct Blast {
    center: usize,
    size: usize,
    steps: usize,
    hue: RgbHue,
}

fn main() {
    let config = config::new();
    let mut params = params::new(
        &config,
        vec!["radius", "threshold"]
    );
    params.sleep /= 10;

    let mut rng = rand::rng();
    let dmx = output::e131::Dmx::new();

    let empty = initializer::luma(config.lights.count(), 0.0);
    let mut blasts: Vec<Blast> = vec![];
    loop {
        let mut pixels = empty.clone();
        if rng.random::<f32>() < params.threshold {
            // add a new blast
            blasts.push(
                Blast {
                    center: rng.random_range(0 .. pixels.len() - 1),
                    size: rng.random_range(15..30),
                    steps: 0,
                    hue: RgbHue::from_degrees(rng.random_range(-180.0 .. 180.0)),
                }
            );
            //println!("{:?}", blasts);
        }
        for blast in blasts.iter_mut() {
            //println!("{:?}", blast);
            blast.steps += 1;
            let radius = blast.steps;
            let value = (blast.size - blast.steps) as f32 / blast.size as f32;
            let left = if radius > blast.center {
                0
            }
            else {
                blast.center - radius
            };
            let right = if radius + blast.center >= pixels.len() {
                pixels.len() - 1
            }
            else {
                radius + blast.center
            };
            for pix in pixels
                .iter_mut()
                .take(right)
                .skip(left) {
                    pix.hue = blast.hue;
                    pix.value = value;
                    pix.saturation = 1.0;
                }
        }
        // remove blasts that have reached their size
        blasts = blasts.iter()
            .filter(|b| b.steps < b.size)
            .cloned()
            .collect();

        output::console::output(&pixels, &config);
        output::e131::output(&dmx, &pixels, &config);
 
        sleep(params.sleep);
    }
}
