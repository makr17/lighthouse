use palette::Hsv;
use rand::Rng;
use rayon::prelude::*;
use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

struct Pixel {
    pix: Hsv,
    lit: bool,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(&config, vec!["step", "threshold"]);

    let mut rng = rand::rng();
    let mut pixels: Vec<Pixel> =
        initializer::luma(config.lights.count(), 0.0)
        .as_slice()
        .par_iter()
        .map(|x| Pixel { pix: *x, lit: false, wax: false })
        .collect();

    let dmx = output::e131::Dmx::new();
    loop {
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);
        for pixel in pixels.as_mut_slice() {
            // already lit?  ramp up or down.
            if pixel.lit {
                if pixel.wax {
                    pixel.pix.value += rng.random_range(0.0 .. params.step);
                    if pixel.pix.value >= config.output.max_intensity {
                        pixel.wax = false;
                    }
                }
                else {
                    pixel.pix.value -= rng.random_range(0.0 .. params.step);
                    if pixel.pix.value <= 0.0 {
                        pixel.pix.value = 0.0;
                        pixel.lit = false;
                        pixel.wax = false;
                    }
                }
            }            
            // random chance to light it
            else {
                // random level to start
                if  rng.random::<f32>() < params.threshold {
                    pixel.lit = true;
                    pixel.wax = true;
                    pixel.pix.value = rng.random_range(0.0 .. params.step);
                }
            }
        }
        
        sleep(params.sleep);
    }
}
