use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

fn main() {
    let mut config = config::new();
	// scale down output max
	// with this pattern _all_ LEDs are lit
	config.output.max_intensity *= 0.6;
    let params = params::new(&config, vec!["colors"]);
    let mut pixels = initializer::gradient(config.lights.count(), params.colors);

    let dmx = output::e131::Dmx::new();
    loop {
        output::console::output(&pixels, &config);
        output::e131::output(&dmx, &pixels, &config);
        // take on pixel from the far right and place it on the left
        // shifting the gradient to the right, ever so slowly
        let pix = pixels.pop().unwrap();
        pixels.insert(0, pix);
        sleep(params.sleep);
    }
}
