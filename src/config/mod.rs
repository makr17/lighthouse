use rayon::prelude::*;
use serde::Deserialize;
use std::env;
use std::fs::File;
use std::io::prelude::*;

#[derive(Clone, Deserialize, Debug)]
pub struct Config {
    pub lights: Lights,
    pub output: Output,
}
#[derive(Clone, Deserialize, Debug)]
pub struct Lights {
    pub zones: Vec<Zone>
}
impl Lights {
    pub fn count(&self) -> usize {
        let count = self.zones.par_iter().map(|x|  x.count()).sum();
        count
    }
}
#[derive(Clone, Deserialize, Debug)]
pub struct Zone {
    pub sections: Vec<Section>
}
impl Zone {
    pub fn count(&self) -> usize {
        let count = self.sections.par_iter().map(|x|  x.count()).sum();
        count
    }
}
#[allow(dead_code)]
#[derive(Clone, Deserialize, Debug)]
pub struct Section {
    pub label:  usize,
    pub head:   usize,
    pub pixels: usize,
    pub tail:   usize,
}
impl Section {
    pub fn count(&self) -> usize {
        self.pixels
    }
}
#[derive(Clone, Deserialize, Debug)]
pub struct Output {
    pub console: bool,
    pub e131:    bool,
    pub universe_size: usize,
    pub max_intensity: f32,
}

pub fn new () -> Config {
    let path = format!("{}/.config/lighthouse/config.toml", env::var("HOME").unwrap());
    let mut config_toml = String::new();
    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(_)  => {
            panic!("Could not find config file!")
        }
    };
    file.read_to_string(&mut config_toml)
        .unwrap_or_else(|err| panic!("Error while reading config: [{}]", err));
    let config: Config = toml::from_str(&config_toml).unwrap();
    //println!("{:?}", config);
    config
}
