use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

fn main() {
    let config = config::new();
    let params = params::new(&config, vec!["colors"]);
    let pixels = initializer::gradient(config.lights.count(), params.colors);

    let dmx = output::e131::Dmx::new();
    loop {
        output::console::output(&pixels, &config);
        output::e131::output(&dmx, &pixels, &config);

        sleep(params.sleep);
    }
}
