use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

fn main() {
    let config = config::new();
    let params = params::new(&config, vec![]);
    let mut pixels = initializer::luma(config.lights.count(), 0.0);

    let dmx = output::e131::Dmx::new();
    loop {
        for idx in 0..config.lights.count() {
            println!("idx={idx}");
            pixels[idx % config.lights.count()].value = 1.0;
            output::console::output(&pixels, &config);
            output::e131::output(&dmx, &pixels, &config);
            println!("");
            sleep(params.sleep);
            pixels[idx % config.lights.count()].value = 0.0;
        }
    }
}
