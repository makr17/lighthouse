use std::thread::sleep;
use std::time::Instant;

use palette::{Hsv, RgbHue};
use rand::prelude::*;
use rayon::prelude::*;

mod config;
mod initializer;
mod output;
mod params;

#[derive(Debug)]
struct Pixel {
    pix: Hsv,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(&config, vec!["sleep"]);

    let min_val = 0.05;
    let max_val = 1.0;
    let mut rng = rand::rng();
    /*
    pattern inspired by the Carnival program in Twinkly lights
    random colors, fading in/out
    when it fades out, it comes back with a new color
     */
    // allocate initial state
    // random hue
    // full saturation
    // random level
    // random choice of initial wax/wane
    let mut pixels = (0 .. config.lights.count())
        .map(|_i| {
            Pixel {
                pix: Hsv::new(
                    rng.random_range(0.0 .. 359.9),
                    1.0,
                    rng.random_range(min_val .. max_val)
                ),
                wax: rand::random(),
            }
        })
        .collect::<Vec<Pixel>>();
             
    let dmx = output::e131::Dmx::new();
    let step = 0.0075;
    loop {
        let now = Instant::now();
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);
        for pixel in pixels.as_mut_slice() {
            if pixel.wax {
                pixel.pix.value += step;
                if pixel.pix.value >= max_val {
                    pixel.wax = false;
                }
            }
            else {
                pixel.pix.value -= step;
                if pixel.pix.value <= min_val {
                    pixel.wax = true;
                    pixel.pix.hue = RgbHue::from_degrees(rng.random_range(0.0 .. 359.9));
                }
            }
        }
        
        sleep(params.sleep - now.elapsed());
    }
}
