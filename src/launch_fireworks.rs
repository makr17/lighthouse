use std::thread::sleep;
use std::time::Instant;

use palette::RgbHue;
use rand::Rng;

mod config;
mod initializer;
mod output;
mod params;

#[derive(Clone, Debug)]
struct Shell {
    target: i32,
    position: i32,
    velocity: i32,
    blast: Blast,
}

#[derive(Clone, Debug)]
struct Blast {
    center: i32,
    max_radius: i32,
    radius: i32,
    hue: RgbHue,
}

fn main() {
    let config = config::new();
    let mut params = params::new(
        &config,
        vec!["radius", "threshold"]
    );
    params.sleep /= 10;

    let mut rng = rand::rng();
    let dmx = output::e131::Dmx::new();

    let empty = initializer::luma(config.lights.count(), 0.0);
    let mut shells: Vec<Shell> = vec![];
    let mut blasts: Vec<Blast> = vec![];
    let velocity: i32 = 5;
    let blast_growth = 6;
    let end = (empty.len() - 1) as i32;
    let middle = (end/2) as i32;
    let mut step: usize = 0;
    loop {
        let now = Instant::now();
        step += 1;
        let mut pixels = empty.clone();
        // should we launch another shell?
        if rng.random::<f32>() < params.threshold {
            // bad things happen at the extremes, avoid, there be dragons
            let target = rng.random_range(velocity..(end-velocity)) as i32;
            // launch from left or right depending on where target is located
            // launch from nearest endpoint
            let (p, v) = if target > middle {
                (end as i32, -velocity)
            }
            else {
                (0, velocity)
            };
            shells.push(
                Shell {
                    target,
                    velocity: v,
                    position: p,
                    blast: Blast {
                        center: target,
                        max_radius: rng.random_range(15..30),
                        radius: 0,
                        hue: RgbHue::from_degrees(rng.random_range(-180.0 .. 180.0)),
                    }
                }
            );
        }

        for blast in blasts.iter_mut() {
            // color in the blast
            // centered on center
            // fading as it expands to radius == size
            let left = if blast.radius > blast.center {
                0
            }
            else {
                (blast.center - blast.radius) as usize
            };
            let right = if blast.radius + blast.center >= pixels.len() as i32 {
                pixels.len() - 1
            }
            else {
                (blast.radius + blast.center) as usize
            };
            let value = (blast.max_radius - blast.radius) as f32 / blast.max_radius as f32;
            for pix in pixels[left..=right].iter_mut() {
                pix.hue = blast.hue;
                pix.value = value;
                pix.saturation = 1.0;
            }
            if step % blast_growth == 0 {
                blast.radius += 1;
            }
        }
        // remove any blasts that are done growing
        blasts = blasts.iter()
            .filter(|b| b.radius < b.max_radius)
            .cloned()
            .collect();

        for s in shells.iter_mut() {
            // white point and tail for the shell
            // make tail length == velocity
            // slow down as we approach target
            //println!("before {:?}", s);
            if s.velocity > 0 {
                for (idx,pix) in pixels[s.position as usize .. (s.position + s.velocity) as usize]
                    .iter_mut()
                    .enumerate() {
                        pix.value = (idx+1) as f32 / s.velocity as f32;
                        pix.saturation = 0.0;
                    }
                s.position += s.velocity;
                // make sure we didn't jump over our target
                if s.position > s.target {
                    s.position = s.target;
                    s.velocity = 0;
                }
            }
            else {
                for (idx,pix) in pixels[(s.position + s.velocity) as usize .. s.position as usize]
                    .iter_mut()
                    .enumerate() {
                        pix.value = (s.velocity + idx as i32) as f32 / s.velocity as f32;
                        pix.saturation = 0.0;
                    }
                s.position += s.velocity;
                // make sure we didn't jump over our target
                if s.position < s.target {
                    s.position = s.target;
                    s.velocity = 0;
                }
            }

            // decelerate as we approach the target
            if s.target != s.position && (s.target - s.position).abs() <= (1 ..= s.velocity.abs()).sum() {
                s.velocity -= s.velocity/s.velocity.abs();
            }
            // if we end up stopped, "jump" to the target
            if s.velocity == 0 {
                s.position = s.target;
                blasts.push(s.blast.clone());
            }
            //println!("after {:?}", s);
        }
        // remove any shells that have reached their target
        shells = shells.iter()
            .filter(|s| s.position != s.target)
            .cloned()
            .collect();

        output::console::output(&pixels, &config);
        output::e131::output(&dmx, &pixels, &config);
 
        if now.elapsed() < params.sleep {
            //println!("sleeping {:?}", params.sleep - now.elapsed());
            sleep(params.sleep - now.elapsed());
        }
    }
}
