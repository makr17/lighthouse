use std::env;
use std::time::Duration;

use getopts::Options;
use palette::{FromColor, Hsv, ShiftHue, Srgb};

use  crate::config;

#[derive(Debug)]
pub struct Params {
    pub colors:    Vec<Hsv>,
    pub radius:    usize,
    pub sleep:     Duration,
    pub step:      f32,
    pub threshold: f32,
}

pub fn new (config: &config::Config, want: Vec<&str>) -> Params {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optopt("s", "sleep", "number of milliseconds to sleep between updates", "200");
    for wanted in &want {
        match *wanted {
            "colors" => {
                opts.optmulti(
                    "c",
                    "colors",
                    "color points for the gradient, in hex",
                    "RRGGBB"
                );
            },
            "radius" => {
                opts.optopt("r", "radius", "radius to search for lit pixels", "20");
            }
            "step" => {
                opts.optopt("p", "step", "max size of random step up/down", "0.2");
            },
            "threshold" => {
                opts.optopt(
                    "t",
                    "threshold",
                    "threshold (0..1] to light pixel",
                    "0.001"
                );
            },
            _ => {},
        }
    }

    let matches = match opts.parse(&args[1..]) {
        Ok(m)  => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };

    let sleep = match matches.opt_str("s") {
        None    => { Duration::new(0, 200 * 1000 * 1000) },
        Some(s) => {
            let n = s.parse::<f64>().unwrap();
            Duration::new(0, (n * 1000_f64 * 1000_f64) as u32)
        },
    };
    
    let start = parse_hex_rgb("FF0000", config.output.max_intensity);
    let opp = start.shift_hue(180.0);
    let end = opp.shift_hue(179.9);
    let mut params = Params {
        sleep,
        colors:    vec![start, opp, end],
        radius:    20,
        step:      0.2,
        threshold: 0.001,
    };

    for wanted in &want {
        match *wanted {
            "colors" => {
                let mut colors = vec![];
                if matches.opt_present("c") {
                    for hex in matches.opt_strs("c") {
                        let hsv = parse_hex_rgb(&hex, config.output.max_intensity);
                        colors.push(hsv);
                    }
                    let start = colors[0];
                    // finish where started so the resulting gradient blends smoothly
                    colors.push(start);
                    params.colors = colors;
                }
            },
            "radius" => {
                let radius = match matches.opt_str("r") {
                    None    => { 20 },
                    Some(s) => { s.parse::<usize>().unwrap() },
                };
                params.radius = radius;
            }
            "step" => {
                let step = match matches.opt_str("p") {
                    None    => { 0.2 },
                    Some(s) => { s.parse::<f32>().unwrap() },
                };
                params.step = step;
            },
            "threshold" => {
                let threshold = match matches.opt_str("t") {
                    None    => { 0.01 },
                    Some(s) => { s.parse::<f32>().unwrap() },
                };
                params.threshold = threshold;
            },
            _ => {},
        }            
    }

    params
}

// parse RGB hex, return HSV with value scaled by scale
fn parse_hex_rgb(hex: &str, scale: f32) -> Hsv {
    let red = match u32::from_str_radix(&hex[0..2], 16) {
        Ok(m)   => (m as f32)/256_f32,
        Err(_f) => 0_f32  // TODO: error?
    };
    let green = match u32::from_str_radix(&hex[2..4], 16) {
        Ok(m)   => (m as f32)/256_f32,
        Err(_f) => 0_f32  // TODO: error?
    };
    let blue = match u32::from_str_radix(&hex[4..6], 16) {
        Ok(m)   => (m as f32)/256_f32,
        Err(_f) => 0_f32  // TODO: error?
    };
    let srgb = Srgb::from_components((red, green, blue));
    let mut hsv = Hsv::from_color(srgb);
    hsv.value *= scale;
    hsv
}
