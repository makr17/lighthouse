use palette::Hsv;
use rand::Rng;
use rayon::prelude::*;
use std::thread::sleep;

mod config;
mod initializer;
mod output;
mod params;

struct Pixel {
    pix: Hsv,
    lit: bool,
    wax: bool,
}

fn main() {
    let config = config::new();
    let params = params::new(
        &config,
        vec!["colors", "radius", "step", "threshold"]
    );
    let mut pixels: Vec<Pixel> =
        initializer::gradient(config.lights.count(), params.colors)
        .as_slice()
        .par_iter()
        .map(|x| Pixel { pix: *x, lit: false, wax: false })
        .collect();
    for pixel in &mut pixels {
        pixel.pix.value = 0.0;
    }

    let mut rng = rand::rng();
    let dmx = output::e131::Dmx::new();
    loop {
        let tmp: Vec<Hsv> = pixels.as_slice().par_iter().map(|x| x.pix).collect();
        output::console::output(&tmp, &config);
        output::e131::output(&dmx, &tmp, &config);

        for i in 0..pixels.len() {
            let mut back = i as i64 - params.radius as i64;
            if back < 0 {
                back = 0;
            }
            let mut forward = i + params.radius;
            if forward >= pixels.len() {
                forward = pixels.len() - 1;
            }
            let modifier: u32 = pixels[(back as usize)..(forward)]
                .par_iter()
                .map(|x| if x.lit { 1 } else { 0 })
                .sum();
            let chance = params.threshold * (modifier + 1) as f32;
            //println!("{} : {} : {} => {}", back, i, forward, chance);
            // already lit?  ramp up or down.
            if pixels[i].lit {
                if pixels[i].wax {
                    pixels[i].pix.value += rng.random_range(0.0 .. params.step);
                    if pixels[i].pix.value >= config.output.max_intensity {
                        pixels[i].wax = false;
                    }
                }
                else {
                    pixels[i].pix.value -= rng.random_range(0.0 .. params.step);
                    if pixels[i].pix.value <= 0.0 {
                        pixels[i].pix.value = 0.0;
                        pixels[i].lit = false;
                        pixels[i].wax = false;
                    }
                }
            }            
            // random chance to light it
            else {
                // random level to start
                if  rng.random::<f32>() < chance {
                    pixels[i].lit = true;
                    pixels[i].wax = true;
                    pixels[i].pix.value = rng.random_range(0.0 .. params.step);
                }
            }
        }        

        sleep(params.sleep);
    }
}
