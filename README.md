# lighthouse

Code to drive the lights trimming our house.

The eaves of the house are trimmed with APA102C strip lighting, 60
RGB LEDs/meter.  The lighting strips are wired into several Pixlite Long
Range Isolated Receivers, which are in turn
connected via ethernet to a Pixlite 16 Long Range MkII.  That is wired
with a crossover cable to a Raspberry Pi v3, which is sitting on the
local wifi network.  The Pi3 emits E1.31 over over the wired network
to the Pixlite controller, which drives the remote receivers, which
drive the light strips.

So, the task at hand is to determine what we want the lights to do,
and to write code to emit E1.31 to make it happen.

So far, the implemented lighting patterns are:

* `lightbow` - gradient rainbow from red through blue to green and back
  to red.  shifts slowly to the right over time.
* `twinkle` - black/white, random chance for pixel to light. once lit,
  ramp up and down in randomly-sized steps.
* `proximity` - similar to twinkle, but lit pixels within a given radius
  boost the probability that a dark pixel will light.  come to think
  of it, `twinkle` is essentially `proximity` with `--radius 0`.
* `gradientprox`, like proximity but the pixels are colored in a
  gradient, ala `lightbow` rather than just white.
* `colorprox` - proximity, but in color.  lit pixels within the radius
  affect the probability a dark pixel will light, and if it does the
  hue is set to be the average of the lit neighbors.  if no neighbors
  hue is set to a random value.
* `particle` - still a work in progress, will eventually be a 1D
  particle simulation.  The idea is that we start with some number of
  particles distributed randomly along the chain.  Each has a hue, a
  velocity and a mass.  Hue and velocity are pretty obvious, mass will
  be represented as intensity.  When two particles collide, some mass
  is consumed and the rest is merged, velocity is adjusted (via
  conservation of momentum), and the color averages between them.
* `tester` - a single pixel walks slowly down the chain.  useful for
  validating the configuration.


configuration file `~/.conf/lighthouse.toml` defines output parameters

```
[output]
console = true
e131    = true
universe_size = 510
max_intensity = 0.7
```

* console - boolean - output to console for testing
* e131 - boolean - output E1.31 over the network to drive lighting controller
* universe_size - integer - how many lights to put in each universe.
  used to chunk a large array of pixels into universe-ish pieces.
* max_intensity - float - max intensity for a pixel lights (so we
  don't blind the neighbors).

also defines the structure of the lights

* lights - contains an array of zones
  * zones - lights are chopped up into zones (e.g. A, B, C), contains
    an array of sections
    * sections - zones are chopped into sections (e.g. 1, 2, 3)
      * label - which section is this
      * head - how many null pixels at the start of the chain
      * pixels - how any pixels to actually light
      * tail - how many null pixels at the end of the chain

in toml, that looks something like

```
[lights]
[[lights.zones]]
  [[lights.zones.sections]]
    label  = 1
    head   = 3
    pixels = 10
    tail   = 3
  [[lights.zones.sections]]
    label  = 2
    head   = 3
    pixels = 20
    tail   = 3
[[lights.zones]]
  [[lights.zones.sections]]
    label  = 1
    head   = 3
    pixels = 15
    tail   = 3
[[lights.zones]]
  [[lights.zones.sections]]
    label  = 1
    head   = 3
    pixels = 10
    tail   = 3
```

three zones, first has two sections, the others only one.
